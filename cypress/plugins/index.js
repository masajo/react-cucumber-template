/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars

/**
 * ? Imports to config Cypress with Cucumber
 */
const cucumber = require('cypress-cucumber-preprocessor').default;
const browserify = require('@cypress/browserify-preprocessor');

module.exports = (on, config) => {

  // * Add Options: Browserify & Typescript TSC to transiple code
  const options = {
    ...browserify.defaultOptions,
    typescript: require.resolve('typescript')
  }

  // Config the execution of cucumber with the options created
  on("file:preprocessor", cucumber(options))

}
