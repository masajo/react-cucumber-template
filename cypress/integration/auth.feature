@auth
Feature: Authentication Login Feature

    Authentication scenarios of our React App

    # Before each scenario
    Background: User is in Login Page
        # Before each scenario Context
        Given the user is in login page


    @login @successful
    Scenario: Successful Admin Login

        # Context
        # Given the user is in login page

        # Actions
        When the user types "Admin" in username field
        # More Actions
        And  the user types "admin123" in password field
        And the user submits login form

        # Verification / Asserts
        Then the app navigates to Home Page
        And the welcome message says Welcome in navbar

