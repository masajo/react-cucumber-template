/**
 * Hello Steps Definitions/Implementations
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';

Given(/Hello/, () => {
    console.log('Given Step definition');
    cy.visit('/');
});

When(/World/, () => {
    console.log('When Step definition');
    cy.get('#txtUsername').type('Admin');
    cy.get('#txtPassword').type('admin123');
    cy.get('#btnLogin').click();
});

Then(/How are you/, () => {
    cy.location('pathname').should('include', 'dashboard')
    console.log('Then Step definition');
});