/**
 * Auth Steps Definitions/Implementations
 */

import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps';

Given('the user is in login page', () => {
    cy.visit('/');
});

When('the user types {string} in username field', (username: string) => {
    console.log(`Types ${username} as username`);
    cy.get('#txtUsername').type(username);
});

And('the user types {string} in password field', (password:string) => {
    console.log(`Types ${password} as username`);
    cy.get('#txtPassword').type(password);
});

And('the user submits login form', () => {
    console.log(`User submits login form`);
    cy.get('#btnLogin').click();
});

Then('the app navigates to Home Page', () => {
    cy.url().should('contains', 'https://opensource-demo.orangehrmlive.com/index.php/dashboard');
});

And('the welcome message says Welcome in navbar', () => {
    // cy.get('#welcome').should('have.text', /Welcome/);
    cy.get('#welcome').should('exist')
});