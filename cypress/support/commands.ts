/**
 * File to write own cypress commands
 */

// Login Command
Cypress.Commands.add('login', (email: string, password: string) => {
    cy.get('.emailInput').type(email);
    cy.get('.passwordInput').type(password);
    cy.get('button').click();
});